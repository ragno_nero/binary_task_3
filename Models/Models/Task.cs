﻿using System;

namespace Model.Models
{
    public partial class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Description { get; set; }
        public DateTime FinishedAt { get; set; }
        public States State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\nName: {Name}\nDescription: {Description}\nCreatedAt: {CreatedAt}\nFinishedAt: {FinishedAt}" +
                $"\nState: {State}\nProjectId: {ProjectId}\nPerformedId: {PerformerId}\n";
        }

        public enum States
        {
            Created = 1,
            Started,
            Finished,
            Canceled
        }
    }
}
