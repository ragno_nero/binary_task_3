﻿using System;

namespace Model.Models
{
    public partial class TaskState
    {
        public int Id { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return $"Id: {Id} Value: {Value}";
        }
    }
}
