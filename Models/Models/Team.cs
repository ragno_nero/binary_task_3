﻿using System;

namespace Model.Models
{
    public partial class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\nName: {Name}\nCreatedAt: {CreatedAt}\n";
        }
    }
}
