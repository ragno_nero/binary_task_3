﻿using System;

namespace Model.Models
{
    public partial class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\nFirstName: {FirstName}\nLastName: {LastName}\nEmail: {Email}\nBirthday: {Birthday}\nRegisteredAt: {RegisteredAt}\nTeamId: {TeamId}\n";
        }
    }
}
