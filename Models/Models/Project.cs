﻿using System;

namespace Model.Models
{
    public partial class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\nName: {Name}\nDescription: {Description}\nCreatedAt: {CreatedAt}\nDeadline: {Deadline}\nAuthorId {AuthorId}\nTeamId: {TeamId}\n";
        }

    }
}
