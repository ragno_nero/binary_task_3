﻿using Model.Models;
using System.Collections.Generic;
using System.Text;

namespace Model.ExtraModels
{
    public partial class UserWithTasks
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder(User.ToString()+"\n  ListOfTasks:\n");
            foreach(var t in Tasks)
            {
                str.AppendLine(t.ToString());
                str.AppendLine("----------");
            }
            return str.ToString();
        }
    }
}
