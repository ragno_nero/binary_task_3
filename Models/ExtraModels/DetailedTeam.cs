﻿using Model.Models;
using System.Collections.Generic;
using System.Text;

namespace Model.ExtraModels
{
    public partial class DetailedTeam
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine($"Id: {Id}\nName: {Name}\n  Users:");
            foreach (var u in Users)
            {
                str.AppendLine(u.ToString()+ "----------");
            }
            return str.ToString();
        }
    }
}
