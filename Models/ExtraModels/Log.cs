﻿using System;

namespace Model.ExtraModels
{
    public class Log
    {
        public string Message { get; set; }
        public DateTime DateTime { get; set; }

        public override string ToString()
        {
            return $"Date and time: {DateTime} Message: {Message}";
        }
    }
}
