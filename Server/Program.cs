﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Server.Hubs;
using Server.RabbitMQ;

namespace Server
{
    public class Program
    {
        public static void RabbitMQReciever()
        {
            Sender _sender = new Sender();
            _sender.Execute("Starting server work!");
            Reciever reciever = new Reciever();
            reciever.Execute();
        }

        public static void Main(string[] args)
        {
            Thread th = new Thread(RabbitMQReciever);
            th.Start();

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
