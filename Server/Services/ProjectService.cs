﻿using Server.DAL;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Server.Services
{
    public class ProjectService : IService<DAL.Models.Project>
    {
        private readonly DALInstance _dal;

        public ProjectService(DALInstance serv)
        {
            _dal = serv;
        }

        public async Task<List<DAL.Models.Project>> GetAllAsync()
        {
            return await _dal.Project.GetAllAsync();
        }

        public async Task<DAL.Models.Project> GetAsync(int id)
        {
            return await _dal.Project.ReadAsync(id);
        }

        public async Task AddAsync(DAL.Models.Project value)
        {
            await _dal.Project.CreateAsync(value);
        }

        public async Task ChangeAsync(int id, DAL.Models.Project value)
        {
            await _dal.Project.UpdateAsync(id, value);
        }

        public async Task DeleteAsync(int id)
        {
            await _dal.Project.DeleteAsync(id);
        }
    }
}
