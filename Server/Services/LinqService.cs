﻿using Microsoft.EntityFrameworkCore;
using Server.DAL.Models.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class LinqService
    {
        private readonly DAL.Models.ApplicationDbContext _db;

        public LinqService(DAL.Models.ApplicationDbContext serv)
        {
            _db = serv;
        }

        public async Task<Dictionary<string, int>> GetUserTaskCountAsync(int id)
        {
            return await _db.Projects.ToDictionaryAsync(p => p.Name, p => _db.Tasks.Where(t => t.PerformerId == id && t.ProjectId == p.Id).Count());
        }

        public async Task<List<DAL.Models.Task>> GetTaskListByUserAsync(int id)
        {
            return await _db.Tasks.Where(t => t.PerformerId == id && t.Name.Length < 45).ToListAsync();
        }

        public async Task<List<Tuple<int, string>>> GetFinishedTasksAsync(int id)
        {
            return await _db.Tasks.Where(t => t.State == (int)States.Finished && t.PerformerId == id && t.FinishedAt.Year == 2019).Select(t => Tuple.Create(t.Id, t.Name)).ToListAsync();
        }

        public async Task<List<DetailedTeam>> GetDetailedTeamAsync()
        {
            return await _db.Teams.GroupJoin(_db.Users, t => t.Id, u => u.TeamId,
                (tm, us) => new DetailedTeam
                {
                    Id = tm.Id,
                    Name = tm.Name,
                    Users = us.Where(u => u.Birthday.AddYears(12) <= DateTime.Now && tm.Id == u.TeamId).OrderByDescending(p => p.RegisteredAt).ToList()
                }
                ).Where(u => u.Users.Count != 0).ToListAsync();
        }

        public async Task<List<UserWithTasks>> GetUserWithTasksAsync()
        {
            return await _db.Users.GroupJoin(_db.Tasks, u => u.Id, t => t.PerformerId,
                (us, ts) => new UserWithTasks
                {
                    User = us,
                    Tasks = ts.Where(t => t.PerformerId == us.Id).OrderByDescending(t => t.Name.Length).ToList()
                }
                ).Where(u => u.Tasks.Count != 0).OrderBy(u => u.User.FirstName).ToListAsync();
        }

        public async Task<DetailedUser> GetDetailedUserAsync(int id)
        {
            DAL.Models.User user = await _db.Users.Where(u => u.Id == id).FirstOrDefaultAsync();
            return await _db.Tasks.Join(_db.Projects, t => t.ProjectId, p => p.Id, (pr, t) => new { pr, t }).Select(y => new DetailedUser
            {
                User = user,
                LastProject = _db.Projects.Select(p => p).Where(t => t.CreatedAt == _db.Projects.Max(d => d.CreatedAt)).SingleOrDefault(),
                TaskCount = _db.Tasks.Where(t => t.ProjectId == _db.Projects.Select(p => p).Where(p => p.CreatedAt == _db.Projects.Max(d => d.CreatedAt)).SingleOrDefault().Id).Count(),
                NotFinishedTaskCount = _db.Tasks.Where(t => t.State != (int)States.Finished && t.PerformerId == user.Id).Count(),
                LongestTask = _db.Tasks.Where(t => t.FinishedAt - t.CreatedAt == _db.Tasks.Max(w => w.FinishedAt - w.CreatedAt)).SingleOrDefault()
            }).FirstOrDefaultAsync();
        }

        public async Task<DetailedProject> GetDetailedProjectAsync(int id)
        {
            DAL.Models.Project project = await _db.Projects.Where(p => p.Id == id).FirstOrDefaultAsync();
            return await _db.Users.Join(_db.Tasks, u => u.Id, t => t.PerformerId,
                (us, ts) => new { us, ts }).Select(y => new DetailedProject
                {
                    Project = project,
                    LongestTask = _db.Tasks.Where(t => t.ProjectId == project.Id && t.Description.Length == _db.Tasks.Where(tt => tt.ProjectId == project.Id).Max(m => m.Description.Length)).FirstOrDefault(),
                    ShortestTask = _db.Tasks.Where(t => t.ProjectId == project.Id && t.Name.Length == _db.Tasks.Where(tt => tt.ProjectId == project.Id).Min(m => m.Name.Length)).FirstOrDefault(),
                    UserCount = _db.Projects.Where(p => p.Description.Length > 25 || _db.Tasks.Where(t => t.ProjectId == p.Id).Count() > 3).GroupJoin(_db.Tasks, p => p.Id, t => t.ProjectId,
                    (p, t) => t.Where(dt => p.Id == dt.ProjectId).Select(s => s.PerformerId)
                    ).ToList().Distinct().Count()
                }
                ).FirstOrDefaultAsync();
        }
        
    }

    public enum States
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}