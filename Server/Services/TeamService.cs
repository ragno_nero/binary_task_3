﻿using Server.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class TeamService : IService<DAL.Models.Team>
    {
        private readonly DALInstance _dal;

        public TeamService(DALInstance dal)
        {
            _dal = dal;
        }
        public async Task AddAsync(DAL.Models.Team value)
        {
            await _dal.Team.CreateAsync(value);
        }

        public async Task ChangeAsync(int id, DAL.Models.Team value)
        {
            await _dal.Team.UpdateAsync(id, value);
        }

        public async Task DeleteAsync(int id)
        {
            await _dal.Team.DeleteAsync(id);
        }

        public async Task<DAL.Models.Team> GetAsync(int id)
        {
            return await _dal.Team.ReadAsync(id);
        }

        public async Task<List<DAL.Models.Team>> GetAllAsync()
        {
            return await _dal.Team.GetAllAsync();
        }
    }
}
