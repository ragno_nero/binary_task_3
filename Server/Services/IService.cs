﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    interface IService<T> where T: class
    {
        Task<List<T>> GetAllAsync();
        Task<T> GetAsync(int id);
        Task AddAsync(T value);
        Task ChangeAsync(int id, T value);
        Task DeleteAsync(int id);
    }
}
