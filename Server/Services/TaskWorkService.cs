﻿using Microsoft.EntityFrameworkCore;
using Server.DAL.Models.Extra;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class TaskWorkService
    {
        private readonly DAL.Models.ApplicationDbContext _db;

        public TaskWorkService(DAL.Models.ApplicationDbContext serv)
        {
            _db = serv;
        }

        public async Task<int> ChangeTaskToCompleted(int id)
        {
            DAL.Models.Task t = await _db.Tasks.Where(tt => tt.Id == id).FirstOrDefaultAsync();
            if (t.State != 3)
            {
                t.State = 3;
                await _db.SaveChangesAsync();
                return t.Id;
            }
            return -1;
        }
    }
}
