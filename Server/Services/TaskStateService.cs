﻿using Server.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class TaskStateService : IService<DAL.Models.TaskState>
    {
        private readonly DALInstance _dal;

        public TaskStateService(DALInstance serv)
        {
            _dal = serv;
        }
        public async Task AddAsync(DAL.Models.TaskState value)
        {
            await _dal.TaskState.CreateAsync(value);
        }

        public async Task ChangeAsync(int id, DAL.Models.TaskState value)
        {
            await _dal.TaskState.UpdateAsync(id, value);
        }

        public async Task DeleteAsync(int id)
        {
            await _dal.TaskState.DeleteAsync(id);
        }

        public async Task<DAL.Models.TaskState> GetAsync(int id)
        {
            return await _dal.TaskState.ReadAsync(id);
        }

        public async Task<List<DAL.Models.TaskState>> GetAllAsync()
        {
            return await _dal.TaskState.GetAllAsync();
        }
    }
}
