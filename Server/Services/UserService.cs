﻿using Server.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class UserService : IService<DAL.Models.User>
    {
        private readonly DALInstance _dal;

        public UserService(DALInstance dal)
        {
            _dal = dal;
        }

        public async Task AddAsync(DAL.Models.User value)
        {
            await _dal.User.CreateAsync(value);
        }

        public async Task ChangeAsync(int id, DAL.Models.User value)
        {
            await _dal.User.UpdateAsync(id,value);
        }

        public async Task DeleteAsync(int id)
        {
            await _dal.User.DeleteAsync(id);
        }

        public async Task<DAL.Models.User> GetAsync(int id)
        {
            return await _dal.User.ReadAsync(id);
        }

        public async Task<List<DAL.Models.User>> GetAllAsync()
        {
            return await _dal.User.GetAllAsync();
        }
    }
}
