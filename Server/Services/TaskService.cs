﻿using Server.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class TaskService : IService<DAL.Models.Task>
    {
        private readonly DALInstance _dal;

        public TaskService(DALInstance serv)
        {
            _dal = serv;
        }
        
        public async Task<List<DAL.Models.Task>> GetAllAsync()
        {
            return await _dal.Task.GetAllAsync();
        }

        public async Task<DAL.Models.Task> GetAsync(int id)
        {
            return await _dal.Task.ReadAsync(id);
        }

        public async Task AddAsync(DAL.Models.Task value)
        {
            await _dal.Task.CreateAsync(value);
        }

        public async Task ChangeAsync(int id, DAL.Models.Task value)
        {
            await _dal.Task.UpdateAsync(id, value);
        }

        public async Task DeleteAsync(int id)
        {
            await _dal.Task.DeleteAsync(id);
        }

    }
}
