﻿using Server.DAL.Models.Extra;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Services
{
    public class FileService
    {
        private const string LogFile = "../logs.txt";
        public async Task<List<Log>> GetFileInformationAsync()
        {
            if (File.Exists(LogFile))
            {
                string[] str = await File.ReadAllLinesAsync(LogFile);
                List<Log> logs = new List<Log>();
                for (int i = 0; i < str.Length; i += 2)
                {
                    Log l = new Log
                    {
                        DateTime = Convert.ToDateTime(str[i]),
                        Message = str[i + 1]
                    };
                    logs.Add(l);
                }
                return logs.OrderByDescending(l=>l.DateTime).ToList();
            }
            return new List<Log> { new Log { DateTime = DateTime.Now, Message = "File is not exist!" } };
        }
    }
}
