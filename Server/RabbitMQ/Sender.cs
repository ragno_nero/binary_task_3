﻿using System;
using RabbitMQ.Client;
using System.Text;

namespace Server.RabbitMQ
{
    public class Sender
    {
        public void Execute(string message)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "Logs", durable: false, exclusive: false, autoDelete: false, arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "", routingKey: "Logs", basicProperties: null, body: body);
            }
        }
    }
}
