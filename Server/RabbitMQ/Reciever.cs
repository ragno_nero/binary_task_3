﻿using Microsoft.AspNetCore.SignalR;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Server.Hubs;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Server.RabbitMQ
{
    public class Reciever
    {
        public static IHubContext<ObserverHub> hubContext;

        public async Task Send(string message)
        {
            if(hubContext != null)
                await hubContext.Clients.All.SendAsync("Notifier", message);
        }

        public void Execute()
        {
            try
            {
                var factory = new ConnectionFactory() { HostName = "localhost" };
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "ResponseLogs", durable: false, exclusive: false, autoDelete: false, arguments: null);

                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        Console.WriteLine(" [x] Received {0}", message);
                        Send(message).Wait();
                        channel.BasicAck(ea.DeliveryTag, true);
                    };
                    channel.BasicConsume(queue: "ResponseLogs", autoAck: false, consumer: consumer);

                    Console.WriteLine(" Press [enter] to exit.");
                    Console.ReadLine();
                }
            }
            catch { }
        }
    }
}
