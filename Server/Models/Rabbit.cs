﻿namespace Server.Models
{
    public class Rabbit
    {
        public string RabbitHost { get; set; }
        public string RabbitQueue { get; set; }
    }
}
