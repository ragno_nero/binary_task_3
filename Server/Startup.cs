﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Server.DAL;
using Server.DAL.Models;
using Server.Hubs;
using Server.Models;
using Server.RabbitMQ;
using Server.Services;

namespace Server
{
    public class Startup
    {
        private static string Connection;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Connection = Configuration.GetConnectionString("Connection");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContext<ApplicationDbContext>(options => 
                options.UseSqlServer(Connection));
            services.AddScoped<DALInstance>();
            services.AddScoped<LinqService>();
            services.AddScoped<ProjectService>();
            services.AddScoped<TaskService>();
            services.AddScoped<TaskStateService>();
            services.AddScoped<TeamService>();
            services.AddScoped<UserService>();
            services.AddScoped<TaskWorkService>();
            services.AddTransient<FileService>();
            services.AddSingleton<Sender>();
            services.AddSingleton<Reciever>();
            services.AddTransient<ObserverHub>();
            services.AddCors(options => options.AddPolicy("CorsPolicy",
            builder =>
            {
                builder.AllowAnyMethod().AllowAnyHeader()
                       .WithOrigins("http://localhost:44304")
                       .AllowCredentials();
            }));
            services.AddSignalR();          
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(builder => builder.AllowAnyOrigin());
            app.UseHttpsRedirection();
            app.UseSignalR(routes =>
            {
                routes.MapHub<ObserverHub>("/observer");
            });
            app.UseMvc();
        }
    }
}
