﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Server.Services;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskWorkController : ControllerBase
    {
        private readonly TaskWorkService _service;
        
        public TaskWorkController(TaskWorkService serv)
        {
            _service = serv;
        }

        // GET api/values
        [HttpGet("{id}")]
        public async Task<ActionResult<int>> Get(int id)
        {
            int retId = await _service.ChangeTaskToCompleted(id);
            if (retId != -1)
                return retId;
            else return BadRequest(-1);
        }
    }
}