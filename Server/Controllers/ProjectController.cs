﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Server.Services;
using Server.RabbitMQ;
using System.Threading.Tasks;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly ProjectService _service;
        private readonly Sender _sender;

        public ProjectController(ProjectService serv, Sender sender)
        {
            _service = serv;
            _sender = sender;
        }
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<List<DAL.Models.Project>>> Get()
        {
            _sender.Execute("ProjectService: Get a list of projects");
            return await _service.GetAllAsync();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DAL.Models.Project>> Get(int id)
        {
            _sender.Execute($"ProjectService: Get a project with id {id}");
            return await _service.GetAsync(id);
        }

        // POST api/values
        [HttpPost]
        public async Task Post([FromBody] DAL.Models.Project value)
        {
            _sender.Execute("ProjectService: Create new project");
            await _service.AddAsync(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] DAL.Models.Project value)
        {
            _sender.Execute($"ProjectService: Update an existing project with id {id}");
            await _service.ChangeAsync(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            _sender.Execute($"ProjectService: Delete a project with id {id}");
            await _service.DeleteAsync(id);
        }
    }
}