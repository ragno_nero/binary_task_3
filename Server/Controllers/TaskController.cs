﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Server.Services;
using Server.RabbitMQ;
using System.Threading.Tasks;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService _service;
        private readonly Sender _sender;

        public TaskController(TaskService serv, Sender sender)
        {
            _service = serv;
            _sender = sender;
        }
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<List<DAL.Models.Task>>> Get()
        {
            _sender.Execute("TaskService: Get a list of tasks");
            return await _service.GetAllAsync();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DAL.Models.Task>> Get(int id)
        {
            _sender.Execute($"TaskService: Get a task with id {id}");
            return await _service.GetAsync(id);
        }

        // POST api/values
        [HttpPost]
        public async Task Post([FromBody] DAL.Models.Task value)
        {
            _sender.Execute("TaskService: Create new task");
            await _service.AddAsync(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] DAL.Models.Task value)
        {
            _sender.Execute($"TaskService: Update an existing task with id {id}");
            await _service.ChangeAsync(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            _sender.Execute($"TaskService: Delete a task with id {id}");
            await _service.DeleteAsync(id);
        }
    }
}