﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Server.DAL.Models.Extra;
using Server.Hubs;
using Server.RabbitMQ;
using Server.Services;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly FileService _service;
        private readonly Sender _sender;
        private readonly IHubContext<ObserverHub> _hubContext;

        public FileController(FileService serv, Sender sender, IHubContext<ObserverHub> hubContext)
        {
            _service = serv;
            _sender = sender;
            _hubContext = hubContext;
            Reciever.hubContext = _hubContext;
        }

        [HttpGet]
        public async Task<ActionResult<List<Log>>> GetLogsAsync()
        {
            _sender.Execute("FileService: Loading all information from file");
            return await _service.GetFileInformationAsync();
        }
    }
}