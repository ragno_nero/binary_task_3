﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Server.Services;
using Server.RabbitMQ;
using System.Threading.Tasks;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStateController : ControllerBase
    {
        private readonly TaskStateService _service;
        private readonly Sender _sender;

        public TaskStateController(TaskStateService serv, Sender sender)
        {
            _service = serv;
            _sender = sender;
        }
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<List<DAL.Models.TaskState>>> Get()
        {
            _sender.Execute("TaskStateService: Get a list of task states");
            return await _service.GetAllAsync();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DAL.Models.TaskState>> Get(int id)
        {
            _sender.Execute($"TaskSateService: Get a task state with id {id}");
            return await _service.GetAsync(id);
        }

        // POST api/values
        [HttpPost]
        public async Task Post([FromBody] DAL.Models.TaskState value)
        {
            _sender.Execute("TaskStateService: Create new task state");
            await _service.AddAsync(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] DAL.Models.TaskState value)
        {
            _sender.Execute($"TaskStateService: Update a task state with id {id}");
            await _service.ChangeAsync(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            _sender.Execute($"TaskStateService: Delete a task with id {id}");
            await _service.DeleteAsync(id);
        }
    }
}