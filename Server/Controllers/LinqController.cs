﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Server.Services;
using Server.RabbitMQ;
using Server.DAL.Models;
using Server.DAL.Models.Extra;
using System.Threading.Tasks;

namespace Server.Controllers
{
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _service;
        private readonly Sender _sender;

        public LinqController(LinqService serv, Sender sender)
        {
            _service = serv;
            _sender = sender;
        }

        [HttpGet]
        [Route("api/[controller]/taskcount/{id}")]
        public async Task<ActionResult<Dictionary<string,int>>> GetNumberOfTasksForUserAsync(int id)
        {
            _sender.Execute("LinQService: Get number of tasks in a user's project");
            return await _service.GetUserTaskCountAsync(id);
        }

        [HttpGet]
        [Route("api/[controller]/usertask/{id}")]
        public async Task<ActionResult<List<DAL.Models.Task>>> GetTaskListByUserAsync(int id)
        {
            _sender.Execute("LinQService: Get a list of tasks assigned for a user");
            return await _service.GetTaskListByUserAsync(id);
        }

        [HttpGet]
        [Route("api/[controller]/finishedtasks/{id}")]
        public async  Task<ActionResult<List<Tuple<int, string>>>> GetFinishedTasksAsync(int id)
        {
            _sender.Execute("LinQService: Get a list of tuples of finished tasks");
            return await _service.GetFinishedTasksAsync(id);
        }

        [HttpGet]
        [Route("api/[controller]/detailedteam")]
        public async Task<ActionResult<List<DetailedTeam>>> GetDetailedTeamAsync()
        {
            _sender.Execute("LinQService: Get a list of detailed information about team");
            return await _service.GetDetailedTeamAsync();
        }

        [HttpGet]
        [Route("api/[controller]/userwithtasks")]
        public async Task<ActionResult<List<UserWithTasks>>> GetUserWithTasks()
        {
            _sender.Execute("LinQService: Get a list of users with their tasks");
            return await _service.GetUserWithTasksAsync();
        }

        [HttpGet]
        [Route("api/[controller]/detaileduser/{id}")]
        public async Task<ActionResult<DetailedUser>> GetDetailedUser(int id)
        {
            _sender.Execute("LinQService: Get a detailed user information");
            return await _service.GetDetailedUserAsync(id);
        }

        [HttpGet]
        [Route("api/[controller]/detailedproject/{id}")]
        public async Task<ActionResult<DetailedProject>> GetDetailedProject(int id)
        {
            _sender.Execute("LinQService: Get a detailed project information");
            return await _service.GetDetailedProjectAsync(id);
        }
    }
}