﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Server.Services;
using Server.RabbitMQ;
using System.Threading.Tasks;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _service;
        private readonly Sender _sender;

        public UserController(UserService serv, Sender sender)
        {
            _service = serv;
            _sender = sender;
        }
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DAL.Models.User>>> Get()
        {
            _sender.Execute("UserService: Get a list of users");
            return await _service.GetAllAsync();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DAL.Models.User>> Get(int id)
        {
            _sender.Execute($"UserService: Get an users with id {id}");
            return await _service.GetAsync(id);
        }

        // POST api/values
        [HttpPost]
        public async Task Post([FromBody] DAL.Models.User value)
        {
            _sender.Execute("UserService: Create new user");
            await _service.AddAsync(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] DAL.Models.User value)
        {
            _sender.Execute($"UserService: Update an existing user with id {id}");
            await _service.ChangeAsync(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            _sender.Execute($"UserService: Delete an user with id {id}");
            await _service.DeleteAsync(id);
        }
    }
}