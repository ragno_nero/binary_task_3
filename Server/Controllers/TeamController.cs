﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Server.Services;
using Server.RabbitMQ;
using System.Threading.Tasks;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly TeamService _service;
        private readonly Sender _sender;

        public TeamController(TeamService serv, Sender sender)
        {
            _service = serv;
            _sender = sender;
        }
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<List<DAL.Models.Team>>> Get()
        {
            _sender.Execute("TeamService: Get a list of teams");
            return await _service.GetAllAsync();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DAL.Models.Team>> Get(int id)
        {
            _sender.Execute($"TeamService: Get a team with id {id}");
            return await _service.GetAsync(id);
        }

        // POST api/values
        [HttpPost]
        public async Task Post([FromBody] DAL.Models.Team value)
        {
            _sender.Execute("TeamService: Create new team");
            await _service.AddAsync(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody] DAL.Models.Team value)
        {
            _sender.Execute($"TeamService: Update an existing team with id {id}");
            await _service.ChangeAsync(id, value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            _sender.Execute($"TeamService: Delete a team with id {id}");
            await _service.DeleteAsync(id);
        }
    }
}