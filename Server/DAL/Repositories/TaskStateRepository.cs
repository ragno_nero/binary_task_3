﻿using Server.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Server.DAL.Repositories
{
    public class TaskStateRepository : IRepository<Models.TaskState>
    {
        private readonly Models.ApplicationDbContext db;

        public TaskStateRepository(Models.ApplicationDbContext context)
        {
            db = context;
        }

        public async Task CreateAsync(Models.TaskState item)
        {
            await db.TaskStates.AddAsync(item);
        }

        public async Task<Models.TaskState> ReadAsync(int id)
        {
            return await db.TaskStates.FindAsync(id);
        }

        public async Task UpdateAsync(int id, Models.TaskState item)
        {
            db.Entry(item).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            Models.TaskState t = await ReadAsync(id);
            if (t != null)
                db.TaskStates.Remove(t);
            await db.SaveChangesAsync();
        }

        public async Task<List<Models.TaskState>> GetAllAsync()
        {
            return await db.TaskStates.ToListAsync();
        }

        public async Task<IEnumerable<Models.TaskState>> FindAsync(Func<Models.TaskState, bool> predicate)
        {
            return db.TaskStates.Where(predicate).ToList();
        }
    }
}
