﻿using Server.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Server.DAL.Repositories
{
    public class ProjectRepository : IRepository<Models.Project>
    {
        private readonly Models.ApplicationDbContext db;

        public ProjectRepository(Models.ApplicationDbContext context)
        {
            db = context;
        }

        public async Task CreateAsync(Models.Project item)
        {
            await db.Projects.AddAsync(item);
            await db.SaveChangesAsync();
        }

        public async Task<Models.Project> ReadAsync(int id)
        {
            return await db.Projects.FindAsync(id);
        }

        public async Task UpdateAsync(int id, Models.Project item)
        {
            db.Entry(item).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            Models.Project p = await ReadAsync(id);
            if (p != null)
                db.Projects.Remove(p);
            await db.SaveChangesAsync();
        }

        public async Task<List<Models.Project>> GetAllAsync()
        {
            return await db.Projects.ToListAsync();
        }

        public async Task<IEnumerable<Models.Project>> FindAsync(Func<Models.Project, bool> predicate)
        {
            return db.Projects.Where(predicate);
        }
    }
}
