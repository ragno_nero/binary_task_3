﻿using Server.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Server.DAL.Repositories
{
    public class TeamRepository : IRepository<Models.Team>
    {
        private readonly Models.ApplicationDbContext db;

        public TeamRepository(Models.ApplicationDbContext context)
        {
            db = context;
        }

        public async Task CreateAsync(Models.Team item)
        {
            await db.Teams.AddAsync(item);
            await db.SaveChangesAsync();
        }

        public async Task<Models.Team> ReadAsync(int id)
        {
            return await db.Teams.FindAsync(id);
        }

        public async Task UpdateAsync(int id, Models.Team item)
        {
            db.Entry(item).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            Models.Team t = await ReadAsync(id);
            if (t != null)
                db.Teams.Remove(t);
            await db.SaveChangesAsync();
        }

        public async Task<List<Models.Team>> GetAllAsync()
        {
            return await db.Teams.ToListAsync();
        }

        public async Task<IEnumerable<Models.Team>> FindAsync(Func<Models.Team, bool> predicate)
        {
            return db.Teams.Where(predicate).ToList();
        }
    }
}
