﻿using Server.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Server.DAL.Repositories
{
    public class TaskRepository : IRepository<Models.Task>
    {
        private readonly Models.ApplicationDbContext db;

        public TaskRepository(Models.ApplicationDbContext context)
        {
            db = context;
        }

        public async Task CreateAsync(Models.Task item)
        {
            await db.Tasks.AddAsync(item);
            await db.SaveChangesAsync();
        }

        public async Task<Models.Task> ReadAsync(int id)
        {
            return await db.Tasks.FindAsync(id);
        }

        public async Task UpdateAsync(int id, Models.Task item)
        {
            db.Entry(item).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            Models.Task t = await ReadAsync(id);
            if (t != null)
                db.Tasks.Remove(t);
            await db.SaveChangesAsync();
        }

        public async Task<List<Models.Task>> GetAllAsync()
        {
            return await db.Tasks.ToListAsync();
        }

        public async Task<IEnumerable<Models.Task>> FindAsync(Func<Models.Task, bool> predicate)
        {
            return db.Tasks.Where(predicate).ToList();
        }
    }
}
