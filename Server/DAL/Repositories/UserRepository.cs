﻿using Server.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Server.DAL.Repositories
{
    public class UserRepository : IRepository<Models.User>
    {
        private readonly Models.ApplicationDbContext db;

        public UserRepository(Models.ApplicationDbContext context)
        {
            db = context;
        }

        public async Task CreateAsync(Models.User item)
        {
            await db.Users.AddAsync(item);
        }

        public async Task<Models.User> ReadAsync(int id)
        {
            return await db.Users.FindAsync(id);
        }

        public async Task UpdateAsync(int id, Models.User item)
        {
            db.Entry(item).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            Models.User u = await ReadAsync(id);
            if (u != null)
                db.Users.Remove(u);
            await db.SaveChangesAsync();
        }

        public async Task<List<Models.User>> GetAllAsync()
        {
            return await db.Users.ToListAsync();
        }

        public async Task<IEnumerable<Models.User>> FindAsync(Func<Models.User, bool> predicate)
        {
            return db.Users.Where(predicate).ToList();
        }
    }
}
