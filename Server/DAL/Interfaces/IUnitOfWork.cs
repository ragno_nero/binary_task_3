﻿using Server.DAL.Models;

namespace Server.DAL.Interfaces
{
    interface IUnitOfWork
    {
        IRepository<Project> Project { get; }
        IRepository<Task> Task { get; }
        IRepository<TaskState> TaskState { get; }
        IRepository<Team> Team { get; }
        IRepository<User> User { get; }
    }
}
