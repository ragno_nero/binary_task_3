﻿using Server.DAL.Interfaces;
using Server.DAL.Repositories;
using System;
using Server.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Server.DAL
{
    public class DALInstance : IUnitOfWork, IDisposable
    {
        private readonly ApplicationDbContext db;
        private ProjectRepository _projectRepository;
        private TaskRepository _taskRepository;
        private TaskStateRepository _taskStateRepository;
        private TeamRepository _teamRepository;
        private UserRepository _userRepository;
        bool disposed = false;

        public DALInstance(DbContextOptions<ApplicationDbContext> options)
        {
            db = new ApplicationDbContext(options);
        }
        public IRepository<Project> Project
        {
            get
            {
                if (_projectRepository == null)
                    _projectRepository = new ProjectRepository(db);
                return _projectRepository;
            }
        }

        public IRepository<Task> Task
        {
            get
            {
                if (_taskRepository == null)
                    _taskRepository = new TaskRepository(db);
                return _taskRepository;
            }
        }

        public IRepository<TaskState> TaskState
        {
            get
            {
                if (_taskStateRepository == null)
                    _taskStateRepository = new TaskStateRepository(db);
                return _taskStateRepository;
            }
        }

        public IRepository<Team> Team
        {
            get
            {
                if (_teamRepository == null)
                    _teamRepository = new TeamRepository(db);
                return _teamRepository;
            }
        }

        public IRepository<User> User
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(db);
                return _userRepository;
            }
        }

        public void Dispose()
        {
            if (disposed == false)
            {
                GC.SuppressFinalize(this);
                disposed = true;
            }
        }
    }
}
