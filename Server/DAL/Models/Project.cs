﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Server.DAL.Models
{
    public partial class Project
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        public DateTime Deadline { get; set; }

        [ForeignKey("Users")]
        [Required]
        public int AuthorId { get; set; }
        [ForeignKey("Teams")]
        [Required]
        public int TeamId { get; set; }
    }
}
