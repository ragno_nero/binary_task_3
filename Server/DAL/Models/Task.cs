﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Server.DAL.Models
{
    public class Task
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public DateTime FinishedAt { get; set; }
        [Required]
        public int State { get; set; }

        [ForeignKey("Projects")]
        public int ProjectId { get; set; }
        [ForeignKey("Users")]
        public int PerformerId { get; set; }
    }
}
