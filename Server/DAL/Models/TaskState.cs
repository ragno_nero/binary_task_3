﻿using System.ComponentModel.DataAnnotations;

namespace Server.DAL.Models
{
    public partial class TaskState
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Value { get; set; }
    }
}
