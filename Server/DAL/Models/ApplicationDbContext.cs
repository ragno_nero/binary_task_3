﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace Server.DAL.Models
{
    public partial class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() { }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        { }

        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<TaskState> TaskStates { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("DataInJson/Projects.json"));
            var tasks = JsonConvert.DeserializeObject<List<Task>>(File.ReadAllText("DataInJson/Tasks.json"));
            var taskstates = JsonConvert.DeserializeObject<List<TaskState>>(File.ReadAllText("DataInJson/TaskStates.json"));
            var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("DataInJson/Teams.json"));
            var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("DataInJson/Users.json"));

            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
            modelBuilder.Entity<TaskState>().HasData(taskstates);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            base.OnModelCreating(modelBuilder);
        }
    }
}
