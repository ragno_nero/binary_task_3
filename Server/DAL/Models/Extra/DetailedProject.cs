﻿namespace Server.DAL.Models.Extra
{
    public partial class DetailedProject
    {
        public Project Project { get; set; }
        public Task LongestTask { get; set; }
        public Task ShortestTask { get; set; }
        public int UserCount { get; set; }
    }
}
