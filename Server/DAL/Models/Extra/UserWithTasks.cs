﻿using System.Collections.Generic;

namespace Server.DAL.Models.Extra
{
    public partial class UserWithTasks
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
