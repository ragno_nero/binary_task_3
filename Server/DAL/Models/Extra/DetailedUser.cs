﻿namespace Server.DAL.Models.Extra
{
    public partial class DetailedUser
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TaskCount { get; set; }
        public int NotFinishedTaskCount { get; set; }
        public Task LongestTask { get; set; }
    }
}
