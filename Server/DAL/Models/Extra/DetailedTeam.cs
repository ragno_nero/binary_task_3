﻿using System.Collections.Generic;

namespace Server.DAL.Models.Extra
{
    public partial class DetailedTeam
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
