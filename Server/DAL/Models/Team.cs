﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Server.DAL.Models
{
    public partial class Team
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
    }
}
