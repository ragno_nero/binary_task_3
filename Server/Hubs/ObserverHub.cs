﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Hubs
{
    public class ObserverHub : Hub
    {
        public override async Task OnConnectedAsync()
        {
            await Send($"{Context.ConnectionId} connected");
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await Send($"{Context.ConnectionId} disconnected");
            await base.OnDisconnectedAsync(exception);
        }

        public async Task Send(string message)
        {
            await Clients.All.SendAsync("Notifier", message);
        }
    }
}
