﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Client.Extensions;

namespace Client
{
    class TaskWorker
    {
        private Communicator _communicator;
        private Random _random;

        public TaskWorker()
        {
            _communicator = new Communicator();
            _random = new Random();
        }

        public async Task<int> MarkRandomTaskWithDelay(int delay)
        {
            var tcs = new TaskCompletionSource<int>();
            var fireAndForgetTask = Task.Delay(delay).ContinueWith(async task => tcs.SetResult(await DoWork()));
            return await tcs.Task;
        }

        private async Task<int> DoWork()
        {
            try
            {
                List<Model.Models.Task> tasks = await _communicator.GetFromApi<List<Model.Models.Task>>(Constants.TASKS);
                int numb = _random.Next(tasks.Count);
                return await _communicator.ChangeTask(tasks[numb].Id);
            }
            catch (AggregateException ae)
            {
                ae.Handle(ex =>
                {
                    if (ex is HttpRequestException)
                        Console.WriteLine("Changing the state is occurred with an error: {0}", ex.Message);
                    return ex is HttpRequestException;
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("Changing the state is occurred with an error: {0}", ex.Message);
            }
            return -1;
        }
    }
}
