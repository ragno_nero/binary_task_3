﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client
{
    public class Constants
    {
        public const string URL = "https://localhost:44304/api/";
        public const string PROJECTS = "Project/";
        public const string TASKS = "Task/";
        public const string TASKSTATES = "TaskState/";
        public const string TASKWORK = "TaskWork/";
        public const string TEAMS = "Team/";
        public const string USERS = "User/";
        public const string LINQ = "Linq/";
        public static string[] ACTIONS = {
            "TaskCount/",
            "UserTask/",
            "FinishedTasks/",
            "DetailedTeam/",
            "UserWithTasks/",
            "DetailedUser/",
            "DetailedProject/"
        };
    }
}
