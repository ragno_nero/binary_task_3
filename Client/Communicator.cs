﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Communicator
    {
        public async Task<T> GetFromApi<T>(string route) where T: class
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var result = await client.GetStringAsync(Constants.URL + route);
                    return JsonConvert.DeserializeObject<T>(result);
                }
            }
            catch (AggregateException ae)
            {
                ae.Handle(ex =>
                {
                    if (ex is HttpRequestException)
                        Console.WriteLine("Error with GET query! Exception: {0}", ex.Message);
                    return ex is HttpRequestException;
                });
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with GET query! Exception {0}", ex.Message);
                return null;
            }
        }

        public async Task PostToApi<T>(T obj, string route)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
            try
            {
                using (HttpClient Client = new HttpClient())
                {
                    var result = await Client.PostAsync(Constants.URL + route, stringContent);
                    result.EnsureSuccessStatusCode();
                    Console.WriteLine("Object is added! Object: {0}", obj);
                }
            }
            catch (AggregateException ae)
            {
                ae.Handle(ex =>
                {
                    if (ex is HttpRequestException)
                        Console.WriteLine("Error with POST query! Exception: {0}", ex.Message);
                    return ex is HttpRequestException;
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with POST query! Exception {0}", ex.Message);
            }
        }

        public async Task PutToApi<T>(T obj, int id, string route)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
            try
            {
                using (HttpClient Client = new HttpClient())
                {
                    var result = await Client.PutAsync(Constants.URL + route + id.ToString(), stringContent);
                    result.EnsureSuccessStatusCode();
                    Console.WriteLine("Object is changed! Obj:\n", obj);
                }
            }
            catch (AggregateException ae)
            {
                ae.Handle(ex =>
                {
                    if (ex is HttpRequestException)
                        Console.WriteLine("Error with PUT query! Exception: {0}", ex.Message);
                    return ex is HttpRequestException;
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with PUT query! Exception {0}", ex.Message);
            }
        }

        public async Task DeleteFromApi(int id, string route)
        {
            try
            {
                using (HttpClient Client = new HttpClient())
                {
                    var result = await Client.DeleteAsync(Constants.URL + route + id);
                    result.EnsureSuccessStatusCode();
                    Console.WriteLine("Object with id={0} deleted!", id);
                }
            }
            catch (AggregateException ae)
            {
                ae.Handle(ex =>
                {
                    if (ex is HttpRequestException)
                        Console.WriteLine("Error with DELETE query! Exception: {0}", ex.Message);
                    return ex is HttpRequestException;
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with DELETE query! Exception {0}", ex.Message);
            }
        }
    }
}
