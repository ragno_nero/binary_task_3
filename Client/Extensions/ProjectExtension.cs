﻿using System;
using Model.Models;

namespace Client.Extensions
{
    public static class ProjectExtension
    {
        public static void SetProject(this Project obj)
        {
            Console.WriteLine("Creating project");
            Console.Write("Enter name: ");
            obj.Name = Console.ReadLine();
            Console.Write("Enter description: ");
            obj.Description = Console.ReadLine();
            Console.Write("Enter date and time of creating: ");
            obj.CreatedAt = Convert.ToDateTime(Console.ReadLine());
            Console.Write("Enter date and time of deadline: ");
            obj.Deadline = Convert.ToDateTime(Console.ReadLine());
            Console.Write("Enter author's id: ");
            obj.AuthorId = Int32.Parse(Console.ReadLine());
            Console.Write("Enter team's id: ");
            obj.TeamId = Int32.Parse(Console.ReadLine());          
        }
    }
}
