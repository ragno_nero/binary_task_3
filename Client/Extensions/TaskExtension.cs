﻿using Model.Models;
using System;

namespace Client.Extensions
{
    static class TaskExtension
    {
        public static void SetTask(this Task obj)
        {
            Console.WriteLine("Creating task");
            Console.Write("Enter name: ");
            obj.Name = Console.ReadLine();
            Console.Write("Enter description: ");
            obj.Description = Console.ReadLine();
            Console.Write("Enter date and time of creating: ");
            obj.CreatedAt = Convert.ToDateTime(Console.ReadLine());
            Console.Write("Enter date and time of deadline: ");
            obj.FinishedAt = Convert.ToDateTime(Console.ReadLine());
            Console.Write("Enter author's id: ");
            obj.ProjectId = Int32.Parse(Console.ReadLine());
            Console.Write("Enter performer's id: ");
            obj.PerformerId = Int32.Parse(Console.ReadLine());
            Console.Write("Enter state: ");
            obj.State = (Task.States)Int32.Parse(Console.ReadLine());           
        }
    }
}
