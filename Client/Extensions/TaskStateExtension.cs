﻿using Model.Models;
using System;

namespace Client.Extensions
{
    public static class TaskStateExtension
    {
        public static void SetTaskState(this TaskState obj)
        {
            Console.WriteLine("Creating task state");
            Console.Write("Enter state: ");
            obj.Value = Console.ReadLine();
        }
    }
}
