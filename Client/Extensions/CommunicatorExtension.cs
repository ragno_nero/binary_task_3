﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Client.Extensions
{
    static class CommunicatorExtension
    {
        public static async Task<int> ChangeTask(this Communicator communicator, int numb)
        {
            using (HttpClient client = new HttpClient())
            {
                var result = await client.GetAsync(Constants.URL + Constants.TASKWORK + numb);
                result.EnsureSuccessStatusCode();
                return JsonConvert.DeserializeObject<int>(await result.Content.ReadAsStringAsync());
            }
        }
    }
}
