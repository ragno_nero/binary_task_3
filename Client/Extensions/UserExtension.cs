﻿using Model.Models;
using System;

namespace Client.Extensions
{
    public static class UserExtension
    {
        public static void SetUser(this User obj)
        {
            Console.WriteLine("Creating user");
            Console.Write("Enter FirstName: ");
            obj.FirstName = Console.ReadLine();
            Console.Write("Enter LastName: ");
            obj.LastName = Console.ReadLine();
            Console.Write("Enter Email: ");
            obj.Email = Console.ReadLine();
            Console.Write("Enter birthday: ");
            obj.Birthday = Convert.ToDateTime(Console.ReadLine());
            Console.Write("Enter register date: ");
            obj.RegisteredAt = Convert.ToDateTime(Console.ReadLine());
            Console.Write("Enter team's id (-1 -> no team): ");
            int? teamId = Int32.Parse(Console.ReadLine());
            obj.TeamId = teamId == -1 ? null : teamId;
        }
    }
}
