﻿using System;
using Model.Models;

namespace Client.Extensions
{
    public static class TeamExtension
    {
        public static void SetTeam(this Team obj)
        {
            Console.WriteLine("Creating team");
            Console.Write("Enter name: ");
            obj.Name = Console.ReadLine();
            Console.Write("Enter date and time of creating: ");
            obj.CreatedAt = Convert.ToDateTime(Console.ReadLine());
        }
    }
}
