﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static TaskWorker TaskWorker = new TaskWorker();
        static void Main(string[] args)
        {
            System.Timers.Timer timer = new System.Timers.Timer(1000);
            timer.AutoReset = true;
            timer.Elapsed += WorkWithTimer;
            timer.Start();

            List<Task> tasks = new List<Task>
            {
                Task.Factory.StartNew(async() => await new Menu().ExecuteAsync())
            };
            while (Task.WhenAll(tasks).Status != TaskStatus.RanToCompletion)
            { }
            timer.Stop();
            timer.Dispose();   
        }

        public async static void WorkWithTimer(Object source, System.Timers.ElapsedEventArgs e)
        {
            
            int id = await TaskWorker.MarkRandomTaskWithDelay(1000);
            Console.WriteLine($"Task with id: {id} has changed to state \'Completed\'");

        }

        public async static Task ConnectToSignalR()
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:44304/observer")
                .AddJsonProtocol(options =>
                {
                    options.PayloadSerializerSettings.ContractResolver =
                        new DefaultContractResolver();
                }).Build();

            await connection.StartAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine("There was an error opening the connection:{0}",
                                      task.Exception.GetBaseException());
                }
                else
                {
                    Console.WriteLine("Connected to SignalR");
                }

            });
            connection.On<string>("Notifier", (message) =>
            {
                Console.WriteLine("Get from server with SignalR: {0}", message);
            });
        }
    }
}
