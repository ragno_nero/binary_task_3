﻿using Client.Extensions;
using Model.ExtraModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client
{
    class Menu
    {
        Communicator _communicator;
        public static bool Cancel { get; private set; }

        public Menu()
        {
            _communicator = new Communicator();
        }

        public async Task ExecuteAsync()
        {
            if (!Cancel)
            {
                bool menu = true;
                do
                {
                    try
                    {
                        int choice = ShowMenu();
                        switch (choice)
                        {
                            case 1:
                                await GetFromOneServerAsync();
                                break;
                            case 2:
                                await GetFromListServerAsync();
                                break;
                            case 3:
                                await SendToServerAsync();
                                break;
                            case 4:
                                await ChangeOnServerAsync();
                                break;
                            case 5:
                                await DeleteFromServerAsync();
                                break;
                            case 6:
                                await ExecuteLinQAsync();
                                break;
                            case 7:
                                List<Log> logs = await _communicator.GetFromApi<List<Log>>("file");
                                PrintList(logs, "Log");
                                break;
                            case 0:
                                menu = false;
                                break;
                        }

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Occurs exception: {0}", ex.Message);
                    }
                    Console.ReadKey();
                    Console.Clear();
                } while (menu == true);
            }
            Cancel = true;
        }


        public async Task ExecuteLinQAsync()
        {
            bool menu = true;
            do
            {
                try
                {
                    int choice = ShowLinQMenu();
                    int id = 0;
                    string path = "";
                    if (choice != 0)
                        path = Constants.LINQ + Constants.ACTIONS[choice - 1];
                    switch (choice)
                    {
                        case 1:
                            id = GetId();
                            Dictionary<string, int> counts = await _communicator.GetFromApi<Dictionary<string, int>>(path + id);
                            if (counts?.Count != 0)
                            {
                                Console.WriteLine("----------------------------------------------------------");

                                foreach (var c in counts)
                                    Console.WriteLine($"Project: {c.Key}, Value: {c.Value}");
                                Console.WriteLine("----------------------------------------------------------");
                            }
                            else Console.WriteLine("Dictionary is empty!");
                            break;
                        case 2:
                            id = GetId();
                            List<Model.Models.Task> tasks = await _communicator.GetFromApi<List<Model.Models.Task>>(path + id);
                            PrintOnConsole(tasks);
                            break;
                        case 3:
                            id = GetId();
                            List<Tuple<int, string>> finished = await _communicator.GetFromApi<List<Tuple<int, string>>>(path + id);
                            PrintOnConsole(finished);
                            break;
                        case 4:
                            List<DetailedTeam> ldt = await _communicator.GetFromApi<List<DetailedTeam>>(path);
                            PrintOnConsole(ldt);
                            break;
                        case 5:
                            List<UserWithTasks> uwt = await _communicator.GetFromApi<List<UserWithTasks>>(path);
                            PrintOnConsole(uwt);
                            break;
                        case 6:
                            id = GetId();
                            DetailedUser du = await _communicator.GetFromApi<DetailedUser>(path + id);
                            if (du != null)
                                Console.WriteLine(du);
                            break;
                        case 7:
                            id = GetId();
                            DetailedProject dp = await _communicator.GetFromApi<DetailedProject>(path + id);
                            if (dp != null)
                                Console.WriteLine(dp);
                            break;
                        case 0:
                            menu = false;
                            break;
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Occurs exception: {0}", ex.Message);
                }
                Console.ReadKey();
                Console.Clear();
            } while (menu == true);

        }

        private void PrintOnConsole<T>(List<T> obj)
        {
            if (obj != null && obj.Count != 0)
            {
                Console.WriteLine("----------------------------------------------------------");
                foreach (var c in obj)
                    Console.WriteLine(c);
                Console.WriteLine("----------------------------------------------------------");
            }
            else Console.WriteLine("List is empty!");
        }

        private int ShowLinQMenu()
        {
            Console.Write("\tLinQ Menu:\n" +
                "1. Get count of tasks for user\n" +
                "2. Get list of tasks for user\n" +
                "3. Get list of finished tasks\n" +
                "4. Get collections of teams\n" +
                "5. Get list of users\n" +
                "6. Get detailed user information\n" +
                "7. Get detailed project information\n" +
                "0. Return to main menu\n" +
                "  Your choice: ");
            int choice = Int32.Parse(Console.ReadLine());
            if (choice < 0 || choice > 7) throw new Exception("Invalid menu item!");
            return choice;
        }

        private int ShowMenu()
        {
            Console.Write("\tMenu:\n" +
                "1. Get an object from server\n" +
                "2. Get objects from server\n" +
                "3. Send to server object\n" +
                "4. Change object on server\n" +
                "5. Delete object from server\n" +
                "6. LinQ\n" +
                "7. Get all infromation from file\n" +
                "0. Exit\n" +
                "  Your choice: ");
            int choice = Int32.Parse(Console.ReadLine());
            if (choice < 0 || choice > 7) throw new Exception("Invalid menu item!");
            return choice;
        }

        private int GetId()
        {
            Console.Write("Enter id: ");
            int id = Int32.Parse(Console.ReadLine());
            return id;
        }

        public async Task GetFromListServerAsync()
        {
            Route r = ChoisedType();
            switch (r)
            {
                case Route.Project:
                    List<Model.Models.Project> lstp = await _communicator.GetFromApi<List<Model.Models.Project>>(Constants.PROJECTS);
                    PrintList(lstp, Constants.PROJECTS.ToLower());
                    break;
                case Route.Task:
                    List<Model.Models.Task> lstt = await _communicator.GetFromApi<List<Model.Models.Task>>(Constants.TASKS);
                    PrintList(lstt, Constants.TASKS.ToLower());
                    break;
                case Route.TaskState:
                    List<Model.Models.TaskState> lstts = await _communicator.GetFromApi<List<Model.Models.TaskState>>(Constants.TASKSTATES);
                    PrintList(lstts, Constants.TASKSTATES.ToLower());
                    break;
                case Route.Team:
                    List<Model.Models.Team> lsttm = await _communicator.GetFromApi<List<Model.Models.Team>>(Constants.TEAMS);
                    PrintList(lsttm, Constants.PROJECTS.ToLower());
                    break;
                case Route.User:
                    List<Model.Models.User> lstu = await _communicator.GetFromApi<List<Model.Models.User>>(Constants.USERS);
                    PrintList(lstu, Constants.USERS.ToLower());
                    break;
                default:
                    break;
            }
        }

        public async Task GetFromOneServerAsync()
        {
            Route r = ChoisedType();
            int id = GetId();
            switch (r)
            {
                case Route.Project:
                    Model.Models.Project p = await _communicator.GetFromApi<Model.Models.Project>(Constants.PROJECTS + id);
                    Writer(p);
                    break;
                case Route.Task:
                    Model.Models.Task t = await _communicator.GetFromApi<Model.Models.Task>(Constants.TASKS + id);
                    Writer(t);
                    break;
                case Route.TaskState:
                    Model.Models.TaskState ts = await _communicator.GetFromApi<Model.Models.TaskState>(Constants.TASKSTATES + id);
                    Writer(ts);
                    break;
                case Route.Team:
                    Model.Models.Team tm = await _communicator.GetFromApi<Model.Models.Team>(Constants.TEAMS + id);
                    Writer(tm);
                    break;
                case Route.User:
                    Model.Models.User u = await _communicator.GetFromApi<Model.Models.User>(Constants.USERS + id);
                    Writer(u);
                    break;
            }
        }

        private void Writer<T>(T obj) where T : class
        {
            if (obj != null)
                Console.WriteLine(obj);
        }

        public async Task SendToServerAsync()
        {
            Route r = ChoisedType();
            switch (r)
            {
                case Route.Project:
                    Model.Models.Project p = new Model.Models.Project();
                    p.SetProject();
                    await _communicator.PostToApi(p, Constants.PROJECTS);
                    break;
                case Route.Task:
                    Model.Models.Task t = new Model.Models.Task();
                    t.SetTask();
                    await _communicator.PostToApi(t, Constants.TASKS);
                    break;
                case Route.TaskState:
                    Model.Models.TaskState ts = new Model.Models.TaskState();
                    ts.SetTaskState();
                    await _communicator.PostToApi(ts, Constants.TASKSTATES);
                    break;
                case Route.Team:
                    Model.Models.Team tm = new Model.Models.Team();
                    tm.SetTeam();
                    await _communicator.PostToApi(tm, Constants.TEAMS);
                    break;
                case Route.User:
                    Model.Models.User us = new Model.Models.User();
                    us.SetUser();
                    await _communicator.PostToApi(us, Constants.USERS);
                    break;
                default:
                    break;
            }
        }

        public async Task ChangeOnServerAsync()
        {
            Route r = ChoisedType();
            int id = GetId();
            switch (r)
            {
                case Route.Project:
                    Model.Models.Project p = await _communicator.GetFromApi<Model.Models.Project>(Constants.PROJECTS + id);
                    if (p != null)
                    {
                        p.SetProject();
                        await _communicator.PutToApi(p, id, Constants.PROJECTS);
                    }
                    else
                        Console.WriteLine("Object is not existing!");
                    break;
                case Route.Task:
                    Model.Models.Task t = await _communicator.GetFromApi<Model.Models.Task>(Constants.TASKS + id);
                    if (t != null)
                    {
                        t.SetTask();
                        await _communicator.PutToApi(t, id, Constants.TASKS);
                    }
                    else
                        Console.WriteLine("Object is not existing!");
                    break;
                case Route.TaskState:
                    Model.Models.TaskState ts = await _communicator.GetFromApi<Model.Models.TaskState>(Constants.TASKSTATES + id);
                    if (ts != null)
                    {
                        ts.SetTaskState();
                        await _communicator.PutToApi(ts, id, Constants.TASKSTATES);
                    }
                    else
                        Console.WriteLine("Object is not existing!");
                    break;
                case Route.Team:
                    Model.Models.Team tm = await _communicator.GetFromApi<Model.Models.Team>(Constants.TEAMS + id);
                    if (tm != null)
                    {
                        tm.SetTeam();
                        await _communicator.PutToApi(tm, id, Constants.TEAMS);
                    }
                    else
                        Console.WriteLine("Object is not existing!");
                    break;
                case Route.User:
                    Model.Models.User us = await _communicator.GetFromApi<Model.Models.User>(Constants.USERS + id);
                    if (us != null)
                    {
                        us.SetUser();
                        await _communicator.PutToApi(us, id, Constants.USERS);
                    }
                    else
                        Console.WriteLine("Object is not existing!");
                    break;
                default:
                    break;
            }
        }

        public async Task DeleteFromServerAsync()
        {
            Route r = ChoisedType();
            int id = GetId();
            switch (r)
            {
                case Route.Project:
                    await _communicator.DeleteFromApi(id, Constants.PROJECTS);
                    break;
                case Route.Task:
                    await _communicator.DeleteFromApi(id, Constants.TASKS);
                    break;
                case Route.TaskState:
                    await _communicator.DeleteFromApi(id, Constants.TASKSTATES);
                    break;
                case Route.Team:
                    await _communicator.DeleteFromApi(id, Constants.TEAMS);
                    break;
                case Route.User:
                    await _communicator.DeleteFromApi(id, Constants.USERS);
                    break;
                default:
                    break;
            }
        }

        private void PrintList<T>(List<T> lst, string type)
        {
            if (lst != null && lst.Count != 0)
            {
                Console.WriteLine($"---Start {type} list---");
                foreach (var l in lst)
                    Console.WriteLine(l);
                Console.WriteLine($"---End {type} list---");
            }
            else
                Console.WriteLine("List is empty");
        }

        public Route ChoisedType()
        {
            Console.Write("\tChoose Menu:\n" +
                "1. Project\n" +
                "2. Task\n" +
                "3. Task State\n" +
                "4. Team\n" +
                "5. User\n" +
                "0. Return to previous\n" +
                "  Your choice: ");
            int choice = Int32.Parse(Console.ReadLine());
            if (choice < 0 || choice > 5) throw new Exception("Invalid menu item!");
            return (Route)choice;
        }
        public enum Route
        {
            Project = 1,
            Task,
            TaskState,
            Team,
            User
        };
    }
}
