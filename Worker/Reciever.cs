﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace Worker
{
    public class Receiver
    {
        FileWorker _fileWorker;
        Sender _sender;

        public Receiver()
        {
            _fileWorker = new FileWorker();
            _sender = new Sender();
        }
        public void Execute()
        {
            try
            {
                var factory = new ConnectionFactory() { HostName = "localhost" };
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "Logs", durable: false, exclusive: false, autoDelete: false, arguments: null);

                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        Console.WriteLine(" [x] Received {0}", message);
                        _fileWorker.WriteToFile(message);
                        _sender.Execute("Successfully received");
                        channel.BasicAck(ea.DeliveryTag, true);
                    };
                    channel.BasicConsume(queue: "Logs", autoAck: false, consumer: consumer);

                    Console.WriteLine(" Press [enter] to exit.");
                    Console.ReadLine();
                }
            }
            catch (Exception ex)
            {
                _sender.Execute($"Failed during handling a message! Exception: {ex.Message}");
            }
        }
    }
}