﻿using Model.ExtraModels;
using System;

namespace Worker
{
    class Program
    {
        static void Main(string[] args)
        {
            Receiver receiver = new Receiver();
            receiver.Execute();

        }
    }
}
