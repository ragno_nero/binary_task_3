﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace Worker
{
    public class Sender
    {
        public void Execute(string message)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "ResponseLogs", durable: false, exclusive: false, autoDelete: false, arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "", routingKey: "ResponseLogs", basicProperties: null, body: body);
            }
        }
    }
}
