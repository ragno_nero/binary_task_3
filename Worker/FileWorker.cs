﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Model.ExtraModels;
using RabbitMQ.Client;

namespace Worker
{
    class FileWorker
    {
        private const string LogFile = "../../../../logs.txt";
        public void WriteToFile(string message)
        {
            StringBuilder str = new StringBuilder();
            str.AppendFormat("{0}{2}{1}{2}", DateTime.Now, message, Environment.NewLine);
            File.AppendAllText(LogFile,str.ToString());
        }
    }
}
